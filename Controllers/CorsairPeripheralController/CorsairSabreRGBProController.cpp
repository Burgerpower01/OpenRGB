#include "CorsairSabreRGBProController.h"
/*-----------------------------------------------------------*\
|  CorsairSabreRGBProController.cpp                           |
|                                                             |
|  Driver for CORSAIR SABRE RGB PRO Gaming Mouse              |
\*-----------------------------------------------------------*/

#include "LogManager.h"

CorsairSabreRGBProController::CorsairSabreRGBProController(hid_device* dev_handle, const char* path)
{
    dev             = dev_handle;
    location        = path;
    LightingControl();
}

CorsairSabreRGBProController::~CorsairSabreRGBProController()
{
    hid_close(dev);
}

std::string CorsairSabreRGBProController::GetDeviceLocation()
{
    return("HID: " + location);
}

std::string CorsairSabreRGBProController::GetFirmwareString()
{
    return "";
}

std::string CorsairSabreRGBProController::GetSerialString()
{
    wchar_t serial_string[128];
    int ret = hid_get_serial_number_string(dev, serial_string, 128);

    if(ret != 0) {
        return("");
    }

    std::wstring return_wstring = serial_string;
    std::string return_string(return_wstring.begin(), return_wstring.end());

    return(return_string);
}

void CorsairSabreRGBProController::SetLEDs(std::vector<RGBColor>colors)
{

    if (colors.size() != 2) {
        return;
    }

    unsigned char usb_buf[SABRE_RGB_PRO_PACKET_LENGTH];
    memset(usb_buf, 0x00, SABRE_RGB_PRO_PACKET_LENGTH);

    usb_buf[0x01] = SABRE_RGB_PRO_WRITE_COMMAND;
    usb_buf[0x02] = 0x06;
    usb_buf[0x04] = 0x0F;
    usb_buf[0x08] = RGBGetRValue(colors[0]);   // Red - Logo
    usb_buf[0x09] = RGBGetRValue(colors[1]);   // Red - Scroll wheel
    usb_buf[0x0B] = 0xFF;
    usb_buf[0x0D] = RGBGetGValue(colors[0]);   // Green - Logo
    usb_buf[0x0E] = RGBGetGValue(colors[1]);   // Green - Scroll wheel
    usb_buf[0x10] = 0xFF;
    usb_buf[0x12] = RGBGetBValue(colors[0]);   // Blue - Logo
    usb_buf[0x13] = RGBGetBValue(colors[1]);   // Blue - Scroll wheel
    usb_buf[0x15] = 0xFF;
    hid_write(dev, usb_buf, SABRE_RGB_PRO_PACKET_LENGTH);
}

void CorsairSabreRGBProController::LightingControl()
{
    /*-----------------------------------------*\
    |  A few init packets have to be sent to    |
    |  enabled software control                 |
    \*-----------------------------------------*/
    unsigned char usb_buf[SABRE_RGB_PRO_PACKET_LENGTH];
    memset(usb_buf, 0x00, SABRE_RGB_PRO_PACKET_LENGTH);

    usb_buf[0x01] = SABRE_RGB_PRO_WRITE_COMMAND;
    usb_buf[0x02] = 0x01;
    usb_buf[0x03] = 0x03;
    usb_buf[0x05] = 0x02;
    hid_write(dev, usb_buf, SABRE_RGB_PRO_PACKET_LENGTH);
    hid_write(dev, usb_buf, SABRE_RGB_PRO_PACKET_LENGTH); // Yes, has to be done 2x

    memset(usb_buf, 0x00, SABRE_RGB_PRO_PACKET_LENGTH);

    usb_buf[0x01] = SABRE_RGB_PRO_WRITE_COMMAND;
    usb_buf[0x02] = 0x0D;
    usb_buf[0x04] = 0x01;
    hid_write(dev, usb_buf, SABRE_RGB_PRO_PACKET_LENGTH); // Seems to be optional
}
