#pragma once

#include "RGBController.h"

#include <string>
#include <hidapi/hidapi.h>

#define SABRE_RGB_PRO_PACKET_LENGTH 128
#define SABRE_RGB_PRO_WRITE_COMMAND 0x08

class CorsairSabreRGBProController
{
public:
    CorsairSabreRGBProController(hid_device* dev_handle, const char* path);
    ~CorsairSabreRGBProController();

    std::string             GetDeviceLocation();
    std::string             GetFirmwareString();
    std::string             GetName();
    std::string             GetSerialString();
    void                    SetLEDs(std::vector<RGBColor> colors);

private:
    hid_device*             dev;

    std::string             firmware_version;
    std::string             location;

    void                    LightingControl();
};
