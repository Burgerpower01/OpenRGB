#include "RGBController_CorsairSabreRGBPro.h"
/*-----------------------------------------------------------*\
|  RGBController_CorsairSabreRGBPro.cpp                       |
|                                                             |
|  Driver for CORSAIR SABRE RGB PRO Gaming Mouse              |
\*-----------------------------------------------------------*/

#include "LogManager.h"

/**------------------------------------------------------------------*\
    @name Corsair Sabre RGB Pro Gaming Mouse
    @category Mouse
    @type USB
    @save :o:
    @direct :white_check_mark:
    @effects :o:
    @detectors DetectCorsairSabreRGBProControllers
    @comment
\*-------------------------------------------------------------------*/

RGBController_CorsairSabreRGBPro::RGBController_CorsairSabreRGBPro(CorsairSabreRGBProController *controller_ptr)
{
    controller          = controller_ptr;
    name                = "CORSAIR SABRE RGB PRO Gaming Mouse";
    vendor              = "Corsair";
    description         = name;
    type                = DEVICE_TYPE_MOUSE;
    version             = controller->GetFirmwareString();
    location            = controller->GetDeviceLocation();
    serial              = controller->GetSerialString();

    mode Direct;
    Direct.name         = "Direct";
    Direct.value        = 0;
    Direct.flags        = MODE_FLAG_HAS_PER_LED_COLOR;
    Direct.color_mode   = MODE_COLORS_PER_LED;
    modes.push_back(Direct);

    SetupZones();
}

RGBController_CorsairSabreRGBPro::~RGBController_CorsairSabreRGBPro()
{
    delete controller;
}

void RGBController_CorsairSabreRGBPro::SetupZones()
{
    unsigned int zone_size  = 2;
    zone mouse_zone;
    mouse_zone.name         = "Mouse Zone";
    mouse_zone.type         = ZONE_TYPE_SINGLE;
    mouse_zone.leds_min     = zone_size;
    mouse_zone.leds_max     = zone_size;
    mouse_zone.leds_count   = zone_size;

    led led_logo;
    led_logo.name = "Logo";
    leds.push_back(led_logo);

    led led_wheel;
    led_wheel.name = "Scroll Wheel";
    leds.push_back(led_wheel);

    zones.push_back(mouse_zone);

    SetupColors();
}

void RGBController_CorsairSabreRGBPro::ResizeZone(int /*zone*/, int /*new_size*/)
{
    /*---------------------------------------------------------*\
    | This device does not support resizing zones               |
    \*---------------------------------------------------------*/
}

void RGBController_CorsairSabreRGBPro::DeviceUpdateLEDs()
{
    controller->SetLEDs(colors);
}

void RGBController_CorsairSabreRGBPro::UpdateZoneLEDs(int /*zone*/)
{
    DeviceUpdateLEDs();
}

void RGBController_CorsairSabreRGBPro::UpdateSingleLED(int /*led*/)
{
    DeviceUpdateLEDs();
}

void RGBController_CorsairSabreRGBPro::DeviceUpdateMode()
{

}
