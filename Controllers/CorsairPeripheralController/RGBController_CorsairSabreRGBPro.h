#pragma once

#include "RGBController.h"
#include "CorsairSabreRGBProController.h"

class RGBController_CorsairSabreRGBPro : public RGBController
{
public:
    RGBController_CorsairSabreRGBPro(CorsairSabreRGBProController *controller_ptr);
    ~RGBController_CorsairSabreRGBPro();

    void                            SetupZones();
    void                            ResizeZone(int zone, int new_size);
    void                            DeviceUpdateLEDs();
    void                            UpdateZoneLEDs(int zone);
    void                            UpdateSingleLED(int led);
    void                            DeviceUpdateMode();

private:
    CorsairSabreRGBProController*   controller;
};
